# Let's do some hacking

//TODO: Add Goal description

## Communication

Zoom+Slack?

//TODO: Add description

## Agenda 

//TODO: Check with Ewa if it will not be better to move it to [Wiki](https://bitbucket.org/ramulus/hackathon-vol2/wiki/Agenda) 

### Day 0 aka pre meeting

1. It should take something around 2 hours
1. During this meeting we would like to go through each idea and maybe discuss it a bit
1. Next we want to check if we can organize teams around these ideas. If it will be hard to do so, we can choice single topic and split around funcional bits.
1. We want to create oportunity to meet peopel a bit better and start using proper communication instead of email

### Day 1

1. Opening 
1. Think about this like about few sprints. Each start with some planning, next coding and demo/testing at the end. 
1. Let's star from tramsforming first idea in ready to develop user stories - 30 minutes
1. Code the simples things and test your set up: repositories, deployment and etc. - 1,30 hour
1. Let's discuss few more user stories and try to code them by the end of the first day - 2 houres

### Day 2

1. Brief first day recap general - 10 minutes
1. In each team check where we are and decide what we want to finish add in the next few houres - 20 minutes
1. Hacking - 2,5/3 houres
1. Demo 
1. Closing

## Technical requierments

There are no many things worth to list in advance and a lot if stuff depend on `pre` meeting, but we think it will be good to establishe some basic rules:

1. To simplify stuff for all memebers all code should based on GIT respoitories.
1. You may decide to you use whatever repo you want, but we uncurage you to use this "Hacking" project to have all everything in the same place.
1. It will be nice and easier if you will be able to define some basic style/coding rules and share with other collegues in advance. In this repo you can find two files: `Settings.Stylecop` and `.editorconfig`, which we are using to share some basic c# coding rules.
1. It will be nice to add a brief descripon of your idea to README
1. By the end of hackathon, your solution may not be fully finshed e.g. may have a dummy integration with external services, but it should be ready to demo
1. It will be nice to give other teams a way to try your app, in such case you may think of some free hosting plan e.g. in Heroku
1. Please keep in mind that this repo is **public one**, however if your team size will be less than 6 you can mark it as private one (BitBucket rules).

## Teams organization

Doesn't matter if we choice single topic to develop or couple of them, we need to split for few teams. Few sugegstions from us:

1. It will be good to have technical and non technical peopel in each team
1. As long as it may sense from technical point of view we want to encurage you to creat cross company teams and avoid situation where all memebers of one hack team comes from single company team.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).